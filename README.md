# WebmTube
## Easy way to manage and share your Webm files collection

You can see working one on https://nenarkosha.ru/webmtube/list.php (master)

### Feature list:
* Simple cataloging your files in database
* Thumbnail generation
* Tags
* Data edit control
* Deleting identical files

### TODO:
* Installation script
* Design

### Installation guide:
1. You need installed packages of **MySQL**, **ffmpeg** and **optipng** for thumbnail generation
2. Clone this repo to location you want to install WebmTube
3. Give www-data (or another web-server user) rights to read and write in this location
4. Create copy of /usr/config.php.examle without ".example" and complete the information about your server
5. Give www-data (or another web-server user) to read from folder with .webm files ($workDir in config)
6. Run the following MyQSL code:

    ```sql
    SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
    SET time_zone = "+00:00";
    
    CREATE TABLE IF NOT EXISTS `queue` (
      `id` bigint(11) NOT NULL AUTO_INCREMENT,
      `filename` text CHARACTER SET latin1 NOT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `id` (`id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
    
    CREATE TABLE IF NOT EXISTS `tags` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `name` text COLLATE utf8_bin NOT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `id` (`id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
    
    CREATE TABLE IF NOT EXISTS `tvalues` (
      `name` char(50) COLLATE utf8_bin NOT NULL,
      `value` float NOT NULL,
      PRIMARY KEY (`name`),
      UNIQUE KEY `name` (`name`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
    
    CREATE TABLE IF NOT EXISTS `usertoken` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `token` text CHARACTER SET latin1 NOT NULL,
      `type` int(11) NOT NULL,
      `banned` tinyint(1) NOT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `id` (`id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
    
    CREATE TABLE IF NOT EXISTS `webm` (
      `id` bigint(11) NOT NULL,
      `name` text COLLATE utf8_bin NOT NULL,
      `descr` text COLLATE utf8_bin NOT NULL,
      `filepath` text COLLATE utf8_bin NOT NULL,
      `thumbnail` text COLLATE utf8_bin NOT NULL,
      `lastEd` text COLLATE utf8_bin,
      UNIQUE KEY `id` (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
    
    CREATE TABLE IF NOT EXISTS `webmtags` (
      `webmid` bigint(11) NOT NULL,
      `tagid` int(11) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
    
    INSERT INTO usertoken (token, type) VALUES ('admin', '1');
    ```

7. Go to /adm/token-admin.php, log in with token "admin", add new token (you will use it) and ban/delete "admin" token for security reasons
8. Then launch /adm/scandir.php and /adm/thumb-generate-queue.php (status of generator you can see at /adm/status.php)
9. WebmTube ready to use

### Crontab for scaning directory with webm and thumbnail generation
Example:
```
* 4 * * * www-data /usr/bin/php /var/www/html/webmtube/adm/scandir.php yoursecretstring
* 5 * * * www-data /usr/bin/php /var/www/html/webmtube/adm/thumb-generate-queue.php yoursecretstring
```
This crontab settings will launch scaning in 4AM and thumbnail generation in 5AM