<?php

	include (__DIR__."/usr/config.php");

	$videoId = $_GET["id"];
	
	$contentType = "video.other";
	$token = str_replace($filterText, "|" ,$_COOKIE["token"]);
	
	
	if ($videoId <= 0)	{
		die ("Invalid video id");
	}

	$query = mysql_query("SELECT * FROM webm WHERE id = $videoId");
	$queryNum = mysql_num_rows($query);

	if ($queryNum <= 0)	{
		die ("Video with id $videoId does not exists on server");
	}

	$videoInfo = mysql_fetch_assoc($query);
	$filePath = $httpWebmFolder.$videoInfo["id"].".webm";
	if ($videoInfo["thumbnail"] == "NULL") {
		$thumbPath = "http://nenarkosha.ru/secret/porasha.png"; #TODO: default thumb pic for not created thumbs, also add check for 404
	}
	else {
		$thumbPath = $videoInfo["thumbnail"].'.small.png';
	}
	$title = "WebmTube - ".$videoInfo["name"];
	$descrOG = mb_convert_encoding($videoInfo['descr'], "UTF-8", "auto");
	include (__DIR__."/templates/html-head-template.php");
	include (__DIR__."/templates/html-header-template.php");
	
?>
<body onload="onLoad();">
<div class='content-container'>
<h2><?php echo $videoInfo['name']; ?></h2>
<video class="player" id="player" width="720px" height="400px" src="<?php echo $filePath; ?>" poster="<?php echo $thumbPath; ?>" preload="auto" controls="controls"></video><br>
<a href="<?php echo $httpDomain.$httpWorkFolder."random.php" ?>">Random</a> 
<?php if ($_GET['rndtag']) { echo "<a href='".$httpDomain.$httpWorkFolder."random.php?tag=".$_GET['rndtag']."'>(by tag)</a> <input type='checkbox' name='ontag' id='ontag' onchange='onTagChange();'> by tag"; } 
	if (!$_GET['rndtag']) {echo "<input type='checkbox' style='visibility: hidden; display: none;' name='ontag' id='ontag' onchange='onTagChange();'>";} ?>
<input type="checkbox" name="autornd" id="autornd" onchange="onAutoChange();"> Autorandom
<input type="checkbox" name="loop" id="loop" onchange="onLoopChange();"> Loop
<input type="checkbox" name="autoplay" id="autoplay" onchange="onAutoplayChange();"> Autoplay <br>
Description: <?php echo $videoInfo['descr']; ?> <br>
Tags: <?php
	$tagQuery = mysql_query("SELECT * FROM webmtags WHERE webmid = '$videoId'");
	while ($curTag = mysql_fetch_assoc($tagQuery)) {
		$tagId = $curTag['tagid'];
		$tagInfoQuery = mysql_query("SELECT * FROM tags WHERE id = '$tagId'");
		$tagInfoQueryRes = mysql_fetch_assoc($tagInfoQuery);
		echo "<a href='".$httpWorkFolder."list.php?tag=$tagId'>".$tagInfoQueryRes['name']."</a> ";
	}
?> <br>
<?php
	if (checkToken(1,2,'')) {
		echo "<a href=".$httpDomain.$httpWorkFolder."edit.php?id=".$videoId.">edit</a>  "; }
	if (checkToken(1,1,'')) {
		?>
		<form class="text-button-form" action="delete.php" method="POST">
			<input type="hidden" name="del" value="0">
			<input type="hidden" name="refer" value="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
			<input type="hidden" name="id" value="<?php echo $videoId; ?>">
			<button  class="text-button">del</button>
		</form>
		<?php
	}
?>
</div>
<script type="text/javascript">
	var videoPlayer = document.getElementById('player');
	var autorndCheck = document.getElementById('autornd');
	var tagCheck = document.getElementById('ontag');
	var autoplayCheck = document.getElementById('autoplay');
	var loopCheck = document.getElementById('loop');
	function getCookie(cname) {
    	var name = cname + "=";
    	var ca = document.cookie.split(';');
    	for(var i = 0; i < ca.length; i++) {
       		var c = ca[i];
       		while (c.charAt(0) == ' ') {
            	c = c.substring(1);
        	}
        	if (c.indexOf(name) == 0) {
            	return c.substring(name.length, c.length);
        	}
   		}
  		return "";
	}

	function onLoad() {
		if (getCookie("volume") != "") {videoPlayer.volume = getCookie("volume");}
		else {videoPlayer.volume = 0.1488;}
		if (getCookie("loop") == "true") {
			loopCheck.checked = true;
			onLoopChange();
		}
		if (getCookie("autornd") == "true") {
			autorndCheck.checked = true;
			onAutoChange();
		}
		if (getCookie("autoplay") == "true") {
			autoplayCheck.checked = true;
			onAutoplayChange();
			videoPlayer.play();
		}
		if (getCookie("tagrnd") == "true") {
			tagCheck.checked = true;
			onAutoChange();
		}
	}
	
	videoPlayer.onvolumechange = function onChangeVolume() {
		document.cookie = "volume="+videoPlayer.volume;
	};

	videoPlayer.onerror = function onVideoLoadError() {
		videoPlayer.poster = "http://nenarkosha.ru/secret/14538490543670.jpg";
		videoPlayer.controls = false;
	};

	videoPlayer.addEventListener('ended', function onVideoEnd() {
		if (autorndCheck.checked) {
			if (tagCheck.checked) {
				location.replace("http://nenarkosha.ru/webmtube/random.php?tag=<?php echo $_GET['rndtag']; ?>");
			}
			else {
			location.replace("http://nenarkosha.ru/webmtube/random.php");			
			}
		}
	});

	function onTagChange() {							
		document.cookie = "tagrnd="+tagCheck.checked;			
	}

	function onAutoChange() {
		if (autorndCheck.checked) {
			loopCheck.checked = false;
			document.cookie = "loop="+loopCheck.checked;
			document.cookie = "autornd="+autorndCheck.checked;
			videoPlayer.loop = false;
			loopCheck.disabled = true;
		}
		else {
			document.cookie = "autornd="+autorndCheck.checked;
			loopCheck.disabled = false;
		}
	}	

	function onLoopChange() {
		if (loopCheck.checked) {
			autorndCheck.checked = false;
			document.cookie = "loop="+loopCheck.checked;
			document.cookie = "autornd="+autorndCheck.checked;
			videoPlayer.loop = true;
			autorndCheck.disabled = true;
		}
		else {
			document.cookie = "loop="+autorndCheck.checked;
			autorndCheck.disabled = false;
			videoPlayer.loop = false;
		}
	}

	function onAutoplayChange() {
		document.cookie = "autoplay="+autoplayCheck.checked;
	}
	
</script>
</body>
</html>