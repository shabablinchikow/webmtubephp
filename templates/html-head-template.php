<?php include (dirname(__DIR__)."/usr/config.php"); global $thumbPath;?>
<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo $httpDomain.$httpWorkFolder; ?>static/main.css">
	<?php if($title){?><meta property="og:title" content="<?php echo $title; ?>" /><?php } ?>
	<?php if($descrOG){?><meta property="og:description" content="<?php echo $descrOG; ?>" /><?php } ?>
	<?php if($contentType){?><meta property="og:type" content="<?php echo $contentType; ?>" /><?php } ?>
	<?php if($thumbPath){?><meta property="og:image" content="<?php echo $httpDomain.$thumbPath; ?>" /> <?php } ?>
	<?php if($filePath){?><meta property="og:video"  content="<?php echo $httpDomain.$filePath; ?>" />
	<meta property="og:video:type" content="video/webm" /><?php } ?>
</head>
